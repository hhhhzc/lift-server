package com.atfuli.elevator.applets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElevatorAppletsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElevatorAppletsApplication.class, args);
    }

}
