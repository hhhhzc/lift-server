package com.atfuli.elevator.applets.service;

import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atfuli.elevator.applets.entity.ElevatorTypeEntity;

import java.util.Map;

/**
 *
 *
 * @author hzc
 * @email
 * @date 2023-01-06 17:35:42
 */
public interface ElevatorTypeService extends IService<ElevatorTypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

