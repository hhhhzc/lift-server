package com.atfuli.elevator.applets.service.impl;

import java.util.Map;

import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.atfuli.elevator.applets.common.utils.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.atfuli.elevator.applets.dao.BookDao;
import com.atfuli.elevator.applets.entity.BookEntity;
import com.atfuli.elevator.applets.service.BookService;
import org.springframework.stereotype.Service;


@Service("BookService")
public class BookServiceImpl extends ServiceImpl<BookDao, BookEntity> implements BookService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<BookEntity> page = this.page(
                new Query<BookEntity>().getPage(params),
                new QueryWrapper<BookEntity>()
        );

        return new PageUtils(page);
    }

}
