package com.atfuli.elevator.applets.service;

import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atfuli.elevator.applets.entity.UserEntity;

import java.util.Map;

/**
 *
 *
 * @author hzc
 * @email
 * @date 2023-01-04 10:48:49
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

