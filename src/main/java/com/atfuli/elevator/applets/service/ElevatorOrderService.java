package com.atfuli.elevator.applets.service;

import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atfuli.elevator.applets.entity.ElevatorOrderEntity;

import java.util.Map;

/**
 *
 *
 * @author hzc
 * @email
 * @date 2023-01-03 13:47:24
 */
public interface ElevatorOrderService extends IService<ElevatorOrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    double calculatePrice(ElevatorOrderEntity elevatorOrder);

    PageUtils queryPageElevator(Map<String, Object> params);

    PageUtils queryPageElevatorType(Map<String, Object> params,Integer type);
}

