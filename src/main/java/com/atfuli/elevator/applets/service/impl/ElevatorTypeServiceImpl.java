package com.atfuli.elevator.applets.service.impl;

import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.atfuli.elevator.applets.common.utils.Query;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.atfuli.elevator.applets.dao.ElevatorTypeDao;
import com.atfuli.elevator.applets.entity.ElevatorTypeEntity;
import com.atfuli.elevator.applets.service.ElevatorTypeService;


@Service("elevatorTypeService")
public class ElevatorTypeServiceImpl extends ServiceImpl<ElevatorTypeDao, ElevatorTypeEntity> implements ElevatorTypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ElevatorTypeEntity> page = this.page(
                new Query<ElevatorTypeEntity>().getPage(params),
                new QueryWrapper<ElevatorTypeEntity>()
        );

        return new PageUtils(page);
    }

}
