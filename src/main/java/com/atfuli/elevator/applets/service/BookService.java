package com.atfuli.elevator.applets.service;

import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atfuli.elevator.applets.entity.BookEntity;

import java.util.Map;

/**
 *
 *
 * @author hzc
 * @email
 * @date 2022-12-09 16:21:17
 */
public interface BookService extends IService<BookEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

