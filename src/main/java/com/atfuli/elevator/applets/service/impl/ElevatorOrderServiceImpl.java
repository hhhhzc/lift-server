package com.atfuli.elevator.applets.service.impl;

import java.util.Map;

import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.atfuli.elevator.applets.common.utils.Query;
import com.atfuli.elevator.applets.dao.ElevatorOrderDao;
import com.atfuli.elevator.applets.entity.ElevatorOrderEntity;
import com.atfuli.elevator.applets.service.ElevatorOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

@Service("elevatorOrderService")
public class ElevatorOrderServiceImpl extends ServiceImpl<ElevatorOrderDao, ElevatorOrderEntity> implements ElevatorOrderService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ElevatorOrderEntity> page = this.page(
                new Query<ElevatorOrderEntity>().getPage(params),
                new QueryWrapper<ElevatorOrderEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public double calculatePrice(ElevatorOrderEntity elevatorOrder) {
        double price = 0;
        if (elevatorOrder.getType() == 1) {
            if (elevatorOrder.getSpeed() == 1.0) {
                if (elevatorOrder.getWeight() == 630) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 11.16;
                            break;
                        case 3:
                            price = 11.47;
                            break;
                        case 4:
                            price = 11.79;
                            break;
                        case 5:
                            price = 12.11;
                            break;
                        case 6:
                            price = 12.42;
                            break;
                        case 7:
                            price = 12.74;
                            break;
                        case 8:
                            price = 13.05;
                            break;
                        case 9:
                            price = 13.37;
                            break;
                        case 10:
                            price = 13.68;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 800) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 11.42;
                            break;
                        case 3:
                            price = 11.74;
                            break;
                        case 4:
                            price = 12.05;
                            break;
                        case 5:
                            price = 12.37;
                            break;
                        case 6:
                            price = 12.69;
                            break;
                        case 7:
                            price = 13.00;
                            break;
                        case 8:
                            price = 13.32;
                            break;
                        case 9:
                            price = 13.63;
                            break;
                        case 10:
                            price = 13.95;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 11.64;
                            break;
                        case 3:
                            price = 11.96;
                            break;
                        case 4:
                            price = 12.27;
                            break;
                        case 5:
                            price = 12.59;
                            break;
                        case 6:
                            price = 12.91;
                            break;
                        case 7:
                            price = 13.22;
                            break;
                        case 8:
                            price = 13.54;
                            break;
                        case 9:
                            price = 13.85;
                            break;
                        case 10:
                            price = 14.17;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1150) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 12.04;
                            break;
                        case 3:
                            price = 12.39;
                            break;
                        case 4:
                            price = 12.75;
                            break;
                        case 5:
                            price = 13.10;
                            break;
                        case 6:
                            price = 13.46;
                            break;
                        case 7:
                            price = 13.81;
                            break;
                        case 8:
                            price = 14.16;
                            break;
                        case 9:
                            price = 14.52;
                            break;
                        case 10:
                            price = 14.87;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1350) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 14.82;
                            break;
                        case 3:
                            price = 15.19;
                            break;
                        case 4:
                            price = 15.56;
                            break;
                        case 5:
                            price = 15.93;
                            break;
                        case 6:
                            price = 16.30;
                            break;
                        case 7:
                            price = 16.67;
                            break;
                        case 8:
                            price = 17.04;
                            break;
                        case 9:
                            price = 17.41;
                            break;
                        case 10:
                            price = 17.78;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1600) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 15.71;
                            break;
                        case 3:
                            price = 16.09;
                            break;
                        case 4:
                            price = 16.48;
                            break;
                        case 5:
                            price = 16.86;
                            break;
                        case 6:
                            price = 17.25;
                            break;
                        case 7:
                            price = 17.63;
                            break;
                        case 8:
                            price = 18.02;
                            break;
                        case 9:
                            price = 18.40;
                            break;
                        case 10:
                            price = 18.79;
                            break;
                    }
                }


            } else if (elevatorOrder.getSpeed() == 1.5) {
                if (elevatorOrder.getWeight() == 630) {
                    switch (elevatorOrder.getLayer()) {
                        case 5:
                            price = 12.24;
                            break;
                        case 6:
                            price = 12.55;
                            break;
                        case 7:
                            price = 12.87;
                            break;
                        case 8:
                            price = 13.18;
                            break;
                        case 9:
                            price = 13.50;
                            break;
                        case 10:
                            price = 13.82;
                            break;
                        case 11:
                            price = 14.27;
                            break;
                        case 12:
                            price = 14.72;
                            break;
                        case 13:
                            price = 15.17;
                            break;
                        case 14:
                            price = 15.62;
                            break;
                        case 15:
                            price = 16.07;
                            break;
                        case 16:
                            price = 16.52;
                            break;
                        case 17:
                            price = 16.97;
                            break;
                        case 18:
                            price = 17.42;
                            break;
                        case 19:
                            price = 17.88;
                            break;
                        case 20:
                            price = 18.33;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 800) {
                    switch (elevatorOrder.getLayer()) {
                        case 5:
                            price = 12.44;
                            break;
                        case 6:
                            price = 12.75;
                            break;
                        case 7:
                            price = 13.07;
                            break;
                        case 8:
                            price = 13.38;
                            break;
                        case 9:
                            price = 13.70;
                            break;
                        case 10:
                            price = 14.01;
                            break;
                        case 11:
                            price = 14.47;
                            break;
                        case 12:
                            price = 14.92;
                            break;
                        case 13:
                            price = 15.37;
                            break;
                        case 14:
                            price = 15.82;
                            break;
                        case 15:
                            price = 16.27;
                            break;
                        case 16:
                            price = 16.72;
                            break;
                        case 17:
                            price = 17.17;
                            break;
                        case 18:
                            price = 17.62;
                            break;
                        case 19:
                            price = 18.07;
                            break;
                        case 20:
                            price = 18.52;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1000) {
                    switch (elevatorOrder.getLayer()) {
                        case 5:
                            price = 12.72;
                            break;
                        case 6:
                            price = 13.04;
                            break;
                        case 7:
                            price = 13.35;
                            break;
                        case 8:
                            price = 13.67;
                            break;
                        case 9:
                            price = 13.98;
                            break;
                        case 10:
                            price = 14.30
                            ;
                            break;
                        case 11:
                            price = 14.75
                            ;
                            break;
                        case 12:
                            price = 15.20
                            ;
                            break;
                        case 13:
                            price = 15.65
                            ;
                            break;
                        case 14:
                            price = 16.10
                            ;
                            break;
                        case 15:
                            price = 16.56
                            ;
                            break;
                        case 16:
                            price = 17.01
                            ;
                            break;
                        case 17:
                            price = 17.46
                            ;
                            break;
                        case 18:
                            price = 17.91
                            ;
                            break;
                        case 19:
                            price = 18.36
                            ;
                            break;
                        case 20:
                            price = 18.81
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1150) {
                    switch (elevatorOrder.getLayer()) {
                        case 5:
                            price = 13.30
                            ;
                            break;
                        case 6:
                            price = 13.65
                            ;
                            break;
                        case 7:
                            price = 14.01
                            ;
                            break;
                        case 8:
                            price = 14.36
                            ;
                            break;
                        case 9:
                            price = 14.72
                            ;
                            break;
                        case 10:
                            price = 15.07
                            ;
                            break;
                        case 11:
                            price = 15.58
                            ;
                            break;
                        case 12:
                            price = 16.08
                            ;
                            break;
                        case 13:
                            price = 16.59
                            ;
                            break;
                        case 14:
                            price = 17.09
                            ;
                            break;
                        case 15:
                            price = 17.60
                            ;
                            break;
                        case 16:
                            price = 18.11
                            ;
                            break;
                        case 17:
                            price = 18.61
                            ;
                            break;
                        case 18:
                            price = 19.12
                            ;
                            break;
                        case 19:
                            price = 19.62
                            ;
                            break;
                        case 20:
                            price = 20.13
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1350) {
                    switch (elevatorOrder.getLayer()) {
                        case 5:
                            price = 16.19
                            ;
                            break;
                        case 6:
                            price = 16.56
                            ;
                            break;
                        case 7:
                            price = 16.93
                            ;
                            break;
                        case 8:
                            price = 17.30
                            ;
                            break;
                        case 9:
                            price = 17.67
                            ;
                            break;
                        case 10:
                            price = 18.04
                            ;
                            break;
                        case 11:
                            price = 18.57
                            ;
                            break;
                        case 12:
                            price = 19.10
                            ;
                            break;
                        case 13:
                            price = 19.62
                            ;
                            break;
                        case 14:
                            price = 20.15
                            ;
                            break;
                        case 15:
                            price = 20.68
                            ;
                            break;
                        case 16:
                            price = 21.21
                            ;
                            break;
                        case 17:
                            price = 21.74
                            ;
                            break;
                        case 18:
                            price = 22.26
                            ;
                            break;
                        case 19:
                            price = 22.79
                            ;
                            break;
                        case 20:
                            price = 23.32
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1600) {
                    switch (elevatorOrder.getLayer()) {
                        case 5:
                            price = 17.24
                            ;
                            break;
                        case 6:
                            price = 17.62
                            ;
                            break;
                        case 7:
                            price = 18.01
                            ;
                            break;
                        case 8:
                            price = 18.39
                            ;
                            break;
                        case 9:
                            price = 18.78
                            ;
                            break;
                        case 10:
                            price = 19.16
                            ;
                            break;
                        case 11:
                            price = 19.71
                            ;
                            break;
                        case 12:
                            price = 20.26
                            ;
                            break;
                        case 13:
                            price = 20.81
                            ;
                            break;
                        case 14:
                            price = 21.36
                            ;
                            break;
                        case 15:
                            price = 21.91
                            ;
                            break;
                        case 16:
                            price = 22.46
                            ;
                            break;
                        case 17:
                            price = 23.01
                            ;
                            break;
                        case 18:
                            price = 23.56
                            ;
                            break;
                        case 19:
                            price = 24.11
                            ;
                            break;
                        case 20:
                            price = 24.66
                            ;
                            break;
                    }
                }

            } else if (elevatorOrder.getSpeed() == 1.75) {
                if (elevatorOrder.getWeight() == 630) {
                    switch (elevatorOrder.getLayer()) {
                        case 10:
                            price = 13.82;
                            break;
                        case 11:
                            price = 14.27;
                            break;
                        case 12:
                            price = 14.72;
                            break;
                        case 13:
                            price = 15.17;
                            break;
                        case 14:
                            price = 15.62;
                            break;
                        case 15:
                            price = 16.07;
                            break;
                        case 16:
                            price = 16.52;
                            break;
                        case 17:
                            price = 16.97;
                            break;
                        case 18:
                            price = 17.42;
                            break;
                        case 19:
                            price = 17.88;
                            break;
                        case 20:
                            price = 18.33;
                            break;
                        case 21:
                            price = 18.78;
                            break;
                        case 22:
                            price = 19.23;
                            break;
                        case 23:
                            price = 19.68;
                            break;
                        case 24:
                            price = 20.13;
                            break;
                        case 25:
                            price = 20.58;
                            break;
                        case 26:
                            price = 21.03;
                            break;
                        case 27:
                            price = 21.48;
                            break;
                        case 28:
                            price = 21.93;
                            break;
                        case 29:
                            price = 22.39;
                            break;
                        case 30:
                            price = 22.84;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 800) {
                    switch (elevatorOrder.getLayer()) {
                        case 10:
                            price = 14.01
                            ;
                            break;
                        case 11:
                            price = 14.47
                            ;
                            break;
                        case 12:
                            price = 14.92
                            ;
                            break;
                        case 13:
                            price = 15.37
                            ;
                            break;
                        case 14:
                            price = 15.82
                            ;
                            break;
                        case 15:
                            price = 16.27
                            ;
                            break;
                        case 16:
                            price = 16.72
                            ;
                            break;
                        case 17:
                            price = 17.17
                            ;
                            break;
                        case 18:
                            price = 17.62
                            ;
                            break;
                        case 19:
                            price = 18.07
                            ;
                            break;
                        case 20:
                            price = 18.52
                            ;
                            break;
                        case 21:
                            price = 18.98
                            ;
                            break;
                        case 22:
                            price = 19.43
                            ;
                            break;
                        case 23:
                            price = 19.88
                            ;
                            break;
                        case 24:
                            price = 20.33
                            ;
                            break;
                        case 25:
                            price = 20.78
                            ;
                            break;
                        case 26:
                            price = 21.23
                            ;
                            break;
                        case 27:
                            price = 21.68
                            ;
                            break;
                        case 28:
                            price = 22.13
                            ;
                            break;
                        case 29:
                            price = 22.58
                            ;
                            break;
                        case 30:
                            price = 23.03
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1000) {
                    switch (elevatorOrder.getLayer()) {
                        case 10:
                            price = 14.43
                            ;
                            break;
                        case 11:
                            price = 14.88
                            ;
                            break;
                        case 12:
                            price = 15.33
                            ;
                            break;
                        case 13:
                            price = 15.79
                            ;
                            break;
                        case 14:
                            price = 16.24
                            ;
                            break;
                        case 15:
                            price = 16.69
                            ;
                            break;
                        case 16:
                            price = 17.14
                            ;
                            break;
                        case 17:
                            price = 17.59
                            ;
                            break;
                        case 18:
                            price = 18.04
                            ;
                            break;
                        case 19:
                            price = 18.49
                            ;
                            break;
                        case 20:
                            price = 18.94
                            ;
                            break;
                        case 21:
                            price = 19.39
                            ;
                            break;
                        case 22:
                            price = 19.84
                            ;
                            break;
                        case 23:
                            price = 20.30
                            ;
                            break;
                        case 24:
                            price = 20.75
                            ;
                            break;
                        case 25:
                            price = 21.20
                            ;
                            break;
                        case 26:
                            price = 21.65
                            ;
                            break;
                        case 27:
                            price = 22.10
                            ;
                            break;
                        case 28:
                            price = 22.55
                            ;
                            break;
                        case 29:
                            price = 23.00
                            ;
                            break;
                        case 30:
                            price = 23.45
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1150) {
                    switch (elevatorOrder.getLayer()) {
                        case 10:
                            price = 15.07
                            ;
                            break;
                        case 11:
                            price = 15.58
                            ;
                            break;
                        case 12:
                            price = 16.08
                            ;
                            break;
                        case 13:
                            price = 16.59
                            ;
                            break;
                        case 14:
                            price = 17.09
                            ;
                            break;
                        case 15:
                            price = 17.60
                            ;
                            break;
                        case 16:
                            price = 18.11
                            ;
                            break;
                        case 17:
                            price = 18.61
                            ;
                            break;
                        case 18:
                            price = 19.12
                            ;
                            break;
                        case 19:
                            price = 19.62
                            ;
                            break;
                        case 20:
                            price = 20.13
                            ;
                            break;
                        case 21:
                            price = 20.64
                            ;
                            break;
                        case 22:
                            price = 21.14
                            ;
                            break;
                        case 23:
                            price = 21.65
                            ;
                            break;
                        case 24:
                            price = 22.15
                            ;
                            break;
                        case 25:
                            price = 22.66
                            ;
                            break;
                        case 26:
                            price = 23.17
                            ;
                            break;
                        case 27:
                            price = 23.67
                            ;
                            break;
                        case 28:
                            price = 24.18
                            ;
                            break;
                        case 29:
                            price = 24.68
                            ;
                            break;
                        case 30:
                            price = 25.19
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1350) {
                    switch (elevatorOrder.getLayer()) {
                        case 10:
                            price = 18.04
                            ;
                            break;
                        case 11:
                            price = 18.57
                            ;
                            break;
                        case 12:
                            price = 19.10
                            ;
                            break;
                        case 13:
                            price = 19.62
                            ;
                            break;
                        case 14:
                            price = 20.15
                            ;
                            break;
                        case 15:
                            price = 20.68
                            ;
                            break;
                        case 16:
                            price = 21.21
                            ;
                            break;
                        case 17:
                            price = 21.74
                            ;
                            break;
                        case 18:
                            price = 22.26
                            ;
                            break;
                        case 19:
                            price = 22.79
                            ;
                            break;
                        case 20:
                            price = 23.32
                            ;
                            break;
                        case 21:
                            price = 23.85
                            ;
                            break;
                        case 22:
                            price = 24.38
                            ;
                            break;
                        case 23:
                            price = 24.90
                            ;
                            break;
                        case 24:
                            price = 25.43
                            ;
                            break;
                        case 25:
                            price = 25.96
                            ;
                            break;
                        case 26:
                            price = 26.49
                            ;
                            break;
                        case 27:
                            price = 27.02
                            ;
                            break;
                        case 28:
                            price = 27.54
                            ;
                            break;
                        case 29:
                            price = 28.07
                            ;
                            break;
                        case 30:
                            price = 28.60
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1600) {
                    switch (elevatorOrder.getLayer()) {
                        case 10:
                            price = 19.16
                            ;
                            break;
                        case 11:
                            price = 19.71
                            ;
                            break;
                        case 12:
                            price = 20.26
                            ;
                            break;
                        case 13:
                            price = 20.81
                            ;
                            break;
                        case 14:
                            price = 21.36
                            ;
                            break;
                        case 15:
                            price = 21.91
                            ;
                            break;
                        case 16:
                            price = 22.46
                            ;
                            break;
                        case 17:
                            price = 23.01
                            ;
                            break;
                        case 18:
                            price = 23.56
                            ;
                            break;
                        case 19:
                            price = 24.11
                            ;
                            break;
                        case 20:
                            price = 24.66
                            ;
                            break;
                        case 21:
                            price = 25.21
                            ;
                            break;
                        case 22:
                            price = 25.76
                            ;
                            break;
                        case 23:
                            price = 26.31
                            ;
                            break;
                        case 24:
                            price = 26.86
                            ;
                            break;
                        case 25:
                            price = 27.41
                            ;
                            break;
                        case 26:
                            price = 27.96
                            ;
                            break;
                        case 27:
                            price = 28.51
                            ;
                            break;
                        case 28:
                            price = 29.06
                            ;
                            break;
                        case 29:
                            price = 29.61
                            ;
                            break;
                        case 30:
                            price = 30.16
                            ;
                            break;
                    }
                }
            } else if (elevatorOrder.getSpeed() == 2.0) {
                if (elevatorOrder.getWeight() == 800) {
                    switch (elevatorOrder.getLayer()) {
                        case 15:
                            price = 19.32
                            ;
                            break;
                        case 16:
                            price = 19.82
                            ;
                            break;
                        case 17:
                            price = 20.33
                            ;
                            break;
                        case 18:
                            price = 20.83
                            ;
                            break;
                        case 19:
                            price = 21.34
                            ;
                            break;
                        case 20:
                            price = 21.85
                            ;
                            break;
                        case 21:
                            price = 22.35
                            ;
                            break;
                        case 22:
                            price = 22.86
                            ;
                            break;
                        case 23:
                            price = 23.36
                            ;
                            break;
                        case 24:
                            price = 23.87
                            ;
                            break;
                        case 25:
                            price = 24.38
                            ;
                            break;
                        case 26:
                            price = 24.88
                            ;
                            break;
                        case 27:
                            price = 25.39
                            ;
                            break;
                        case 28:
                            price = 25.89
                            ;
                            break;
                        case 29:
                            price = 26.40
                            ;
                            break;
                        case 30:
                            price = 26.91
                            ;
                            break;
                        case 31:
                            price = 27.41
                            ;
                            break;
                        case 32:
                            price = 27.92
                            ;
                            break;
                        case 33:
                            price = 28.42
                            ;
                            break;
                        case 34:
                            price = 28.93
                            ;
                            break;
                        case 35:
                            price = 29.44
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1000) {
                    switch (elevatorOrder.getLayer()) {
                        case 15:
                            price = 19.67
                            ;
                            break;
                        case 16:
                            price = 20.17
                            ;
                            break;
                        case 17:
                            price = 20.68
                            ;
                            break;
                        case 18:
                            price = 21.19
                            ;
                            break;
                        case 19:
                            price = 21.69
                            ;
                            break;
                        case 20:
                            price = 22.20
                            ;
                            break;
                        case 21:
                            price = 22.70
                            ;
                            break;
                        case 22:
                            price = 23.21
                            ;
                            break;
                        case 23:
                            price = 23.72
                            ;
                            break;
                        case 24:
                            price = 24.22
                            ;
                            break;
                        case 25:
                            price = 24.73
                            ;
                            break;
                        case 26:
                            price = 25.23
                            ;
                            break;
                        case 27:
                            price = 25.74
                            ;
                            break;
                        case 28:
                            price = 26.25
                            ;
                            break;
                        case 29:
                            price = 26.75
                            ;
                            break;
                        case 30:
                            price = 27.26
                            ;
                            break;
                        case 31:
                            price = 27.76
                            ;
                            break;
                        case 32:
                            price = 28.27
                            ;
                            break;
                        case 33:
                            price = 28.78
                            ;
                            break;
                        case 34:
                            price = 29.28
                            ;
                            break;
                        case 35:
                            price = 29.79
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1150) {
                    switch (elevatorOrder.getLayer()) {
                        case 15:
                            price = 20.72
                            ;
                            break;
                        case 16:
                            price = 21.25
                            ;
                            break;
                        case 17:
                            price = 21.78
                            ;
                            break;
                        case 18:
                            price = 22.31
                            ;
                            break;
                        case 19:
                            price = 22.84
                            ;
                            break;
                        case 20:
                            price = 23.36
                            ;
                            break;
                        case 21:
                            price = 23.89
                            ;
                            break;
                        case 22:
                            price = 24.42
                            ;
                            break;
                        case 23:
                            price = 24.95
                            ;
                            break;
                        case 24:
                            price = 25.48
                            ;
                            break;
                        case 25:
                            price = 26.00
                            ;
                            break;
                        case 26:
                            price = 26.53
                            ;
                            break;
                        case 27:
                            price = 27.06
                            ;
                            break;
                        case 28:
                            price = 27.59
                            ;
                            break;
                        case 29:
                            price = 28.12
                            ;
                            break;
                        case 30:
                            price = 28.64
                            ;
                            break;
                        case 31:
                            price = 29.17
                            ;
                            break;
                        case 32:
                            price = 29.70
                            ;
                            break;
                        case 33:
                            price = 30.23
                            ;
                            break;
                        case 34:
                            price = 30.76
                            ;
                            break;
                        case 35:
                            price = 31.28
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1350) {
                    switch (elevatorOrder.getLayer()) {
                        case 15:
                            price = 21.91
                            ;
                            break;
                        case 16:
                            price = 22.48
                            ;
                            break;
                        case 17:
                            price = 23.06
                            ;
                            break;
                        case 18:
                            price = 23.63
                            ;
                            break;
                        case 19:
                            price = 24.20
                            ;
                            break;
                        case 20:
                            price = 24.77
                            ;
                            break;
                        case 21:
                            price = 25.34
                            ;
                            break;
                        case 22:
                            price = 25.92
                            ;
                            break;
                        case 23:
                            price = 26.49
                            ;
                            break;
                        case 24:
                            price = 27.06
                            ;
                            break;
                        case 25:
                            price = 27.63
                            ;
                            break;
                        case 26:
                            price = 28.20
                            ;
                            break;
                        case 27:
                            price = 28.78
                            ;
                            break;
                        case 28:
                            price = 29.35
                            ;
                            break;
                        case 29:
                            price = 29.92
                            ;
                            break;
                        case 30:
                            price = 30.49
                            ;
                            break;
                        case 31:
                            price = 31.06
                            ;
                            break;
                        case 32:
                            price = 31.64
                            ;
                            break;
                        case 33:
                            price = 32.21
                            ;
                            break;
                        case 34:
                            price = 32.78
                            ;
                            break;
                        case 35:
                            price = 33.35
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1600) {
                    switch (elevatorOrder.getLayer()) {
                        case 15:
                            price = 23.80
                            ;
                            break;
                        case 16:
                            price = 24.38
                            ;
                            break;
                        case 17:
                            price = 24.95
                            ;
                            break;
                        case 18:
                            price = 25.52
                            ;
                            break;
                        case 19:
                            price = 26.09
                            ;
                            break;
                        case 20:
                            price = 26.66
                            ;
                            break;
                        case 21:
                            price = 27.24
                            ;
                            break;
                        case 22:
                            price = 27.81
                            ;
                            break;
                        case 23:
                            price = 28.38
                            ;
                            break;
                        case 24:
                            price = 28.95
                            ;
                            break;
                        case 25:
                            price = 29.52
                            ;
                            break;
                        case 26:
                            price = 30.10
                            ;
                            break;
                        case 27:
                            price = 30.67
                            ;
                            break;
                        case 28:
                            price = 31.24
                            ;
                            break;
                        case 29:
                            price = 31.81
                            ;
                            break;
                        case 30:
                            price = 32.38
                            ;
                            break;
                        case 31:
                            price = 32.96
                            ;
                            break;
                        case 32:
                            price = 33.53
                            ;
                            break;
                        case 33:
                            price = 34.10
                            ;
                            break;
                        case 34:
                            price = 34.67
                            ;
                            break;
                        case 35:
                            price = 35.24
                            ;
                            break;
                    }
                }


            } else if (elevatorOrder.getSpeed() == 2.5) {
                if (elevatorOrder.getWeight() == 800) {
                    switch (elevatorOrder.getLayer()) {
                        case 20:
                            price = 22.22
                            ;
                            break;
                        case 21:
                            price = 22.73
                            ;
                            break;
                        case 22:
                            price = 23.23
                            ;
                            break;
                        case 23:
                            price = 23.74
                            ;
                            break;
                        case 24:
                            price = 24.24
                            ;
                            break;
                        case 25:
                            price = 24.75
                            ;
                            break;
                        case 26:
                            price = 25.26
                            ;
                            break;
                        case 27:
                            price = 25.76
                            ;
                            break;
                        case 28:
                            price = 26.27
                            ;
                            break;
                        case 29:
                            price = 26.77
                            ;
                            break;
                        case 30:
                            price = 27.28
                            ;
                            break;
                        case 31:
                            price = 27.79
                            ;
                            break;
                        case 32:
                            price = 28.29
                            ;
                            break;
                        case 33:
                            price = 28.80
                            ;
                            break;
                        case 34:
                            price = 29.30
                            ;
                            break;
                        case 35:
                            price = 29.81
                            ;
                            break;
                        case 36:
                            price = 30.32
                            ;
                            break;
                        case 37:
                            price = 30.82
                            ;
                            break;
                        case 38:
                            price = 31.33
                            ;
                            break;
                        case 39:
                            price = 31.83
                            ;
                            break;
                        case 40:
                            price = 32.34
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1000) {
                    switch (elevatorOrder.getLayer()) {
                        case 20:
                            price = 22.75
                            ;
                            break;
                        case 21:
                            price = 23.25
                            ;
                            break;
                        case 22:
                            price = 23.76
                            ;
                            break;
                        case 23:
                            price = 24.27
                            ;
                            break;
                        case 24:
                            price = 24.77
                            ;
                            break;
                        case 25:
                            price = 25.28
                            ;
                            break;
                        case 26:
                            price = 25.78
                            ;
                            break;
                        case 27:
                            price = 26.29
                            ;
                            break;
                        case 28:
                            price = 26.80
                            ;
                            break;
                        case 29:
                            price = 27.30
                            ;
                            break;
                        case 30:
                            price = 27.81
                            ;
                            break;
                        case 31:
                            price = 28.31
                            ;
                            break;
                        case 32:
                            price = 28.82
                            ;
                            break;
                        case 33:
                            price = 29.33
                            ;
                            break;
                        case 34:
                            price = 29.83
                            ;
                            break;
                        case 35:
                            price = 30.34
                            ;
                            break;
                        case 36:
                            price = 30.84
                            ;
                            break;
                        case 37:
                            price = 31.35
                            ;
                            break;
                        case 38:
                            price = 31.86
                            ;
                            break;
                        case 39:
                            price = 32.36
                            ;
                            break;
                        case 40:
                            price = 32.87
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1150) {
                    switch (elevatorOrder.getLayer()) {
                        case 20:
                            price = 23.72
                            ;
                            break;
                        case 21:
                            price = 24.24
                            ;
                            break;
                        case 22:
                            price = 24.77
                            ;
                            break;
                        case 23:
                            price = 25.30
                            ;
                            break;
                        case 24:
                            price = 25.83
                            ;
                            break;
                        case 25:
                            price = 26.36
                            ;
                            break;
                        case 26:
                            price = 26.88
                            ;
                            break;
                        case 27:
                            price = 27.41
                            ;
                            break;
                        case 28:
                            price = 27.94
                            ;
                            break;
                        case 29:
                            price = 28.47
                            ;
                            break;
                        case 30:
                            price = 29.00
                            ;
                            break;
                        case 31:
                            price = 29.52
                            ;
                            break;
                        case 32:
                            price = 30.05
                            ;
                            break;
                        case 33:
                            price = 30.58
                            ;
                            break;
                        case 34:
                            price = 31.11
                            ;
                            break;
                        case 35:
                            price = 31.64
                            ;
                            break;
                        case 36:
                            price = 32.16
                            ;
                            break;
                        case 37:
                            price = 32.69
                            ;
                            break;
                        case 38:
                            price = 33.22
                            ;
                            break;
                        case 39:
                            price = 33.75
                            ;
                            break;
                        case 40:
                            price = 34.28
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1350) {
                    switch (elevatorOrder.getLayer()) {
                        case 20:
                            price = 25.83
                            ;
                            break;
                        case 21:
                            price = 26.40
                            ;
                            break;
                        case 22:
                            price = 26.97
                            ;
                            break;
                        case 23:
                            price = 27.54
                            ;
                            break;
                        case 24:
                            price = 28.12
                            ;
                            break;
                        case 25:
                            price = 28.69
                            ;
                            break;
                        case 26:
                            price = 29.26
                            ;
                            break;
                        case 27:
                            price = 29.83
                            ;
                            break;
                        case 28:
                            price = 30.40
                            ;
                            break;
                        case 29:
                            price = 30.98
                            ;
                            break;
                        case 30:
                            price = 31.55
                            ;
                            break;
                        case 31:
                            price = 32.12
                            ;
                            break;
                        case 32:
                            price = 32.69
                            ;
                            break;
                        case 33:
                            price = 33.26
                            ;
                            break;
                        case 34:
                            price = 33.84
                            ;
                            break;
                        case 35:
                            price = 34.41
                            ;
                            break;
                        case 36:
                            price = 34.98
                            ;
                            break;
                        case 37:
                            price = 35.55
                            ;
                            break;
                        case 38:
                            price = 36.12
                            ;
                            break;
                        case 39:
                            price = 36.70
                            ;
                            break;
                        case 40:
                            price = 37.27
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1600) {
                    switch (elevatorOrder.getLayer()) {
                        case 20:
                            price = 27.83
                            ;
                            break;
                        case 21:
                            price = 28.41
                            ;
                            break;
                        case 22:
                            price = 29.00
                            ;
                            break;
                        case 23:
                            price = 29.58
                            ;
                            break;
                        case 24:
                            price = 30.16
                            ;
                            break;
                        case 25:
                            price = 30.75
                            ;
                            break;
                        case 26:
                            price = 31.33
                            ;
                            break;
                        case 27:
                            price = 31.91
                            ;
                            break;
                        case 28:
                            price = 32.49
                            ;
                            break;
                        case 29:
                            price = 33.08
                            ;
                            break;
                        case 30:
                            price = 33.66
                            ;
                            break;
                        case 31:
                            price = 34.24
                            ;
                            break;
                        case 32:
                            price = 34.83
                            ;
                            break;
                        case 33:
                            price = 35.41
                            ;
                            break;
                        case 34:
                            price = 35.99
                            ;
                            break;
                        case 35:
                            price = 36.58
                            ;
                            break;
                        case 36:
                            price = 37.16
                            ;
                            break;
                        case 37:
                            price = 37.74
                            ;
                            break;
                        case 38:
                            price = 38.32
                            ;
                            break;
                        case 39:
                            price = 38.91
                            ;
                            break;
                        case 40:
                            price = 39.49
                            ;
                            break;
                    }
                }


            } else if (elevatorOrder.getSpeed() == 3.0) {
                if (elevatorOrder.getWeight() == 800) {
                    switch (elevatorOrder.getLayer()) {
                        case 30:
                            price = 36.52
                            ;
                            break;
                        case 31:
                            price = 37.14
                            ;
                            break;
                        case 32:
                            price = 37.75
                            ;
                            break;
                        case 33:
                            price = 38.37
                            ;
                            break;
                        case 34:
                            price = 38.98
                            ;
                            break;
                        case 35:
                            price = 39.60
                            ;
                            break;
                        case 36:
                            price = 40.22
                            ;
                            break;
                        case 37:
                            price = 40.83
                            ;
                            break;
                        case 38:
                            price = 41.45
                            ;
                            break;
                        case 39:
                            price = 42.06
                            ;
                            break;
                        case 40:
                            price = 42.68
                            ;
                            break;
                        case 41:
                            price = 43.30
                            ;
                            break;
                        case 42:
                            price = 43.91
                            ;
                            break;
                        case 43:
                            price = 44.53
                            ;
                            break;
                        case 44:
                            price = 45.14
                            ;
                            break;
                        case 45:
                            price = 45.76
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1000) {
                    switch (elevatorOrder.getLayer()) {
                        case 30:
                            price = 38.72
                            ;
                            break;
                        case 31:
                            price = 39.36
                            ;
                            break;
                        case 32:
                            price = 40.00
                            ;
                            break;
                        case 33:
                            price = 40.63
                            ;
                            break;
                        case 34:
                            price = 41.27
                            ;
                            break;
                        case 35:
                            price = 41.91
                            ;
                            break;
                        case 36:
                            price = 42.55
                            ;
                            break;
                        case 37:
                            price = 43.19
                            ;
                            break;
                        case 38:
                            price = 43.82
                            ;
                            break;
                        case 39:
                            price = 44.46
                            ;
                            break;
                        case 40:
                            price = 45.10
                            ;
                            break;
                        case 41:
                            price = 45.74
                            ;
                            break;
                        case 42:
                            price = 46.38
                            ;
                            break;
                        case 43:
                            price = 47.01
                            ;
                            break;
                        case 44:
                            price = 47.65
                            ;
                            break;
                        case 45:
                            price = 48.29
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1150) {
                    switch (elevatorOrder.getLayer()) {
                        case 30:
                            price = 39.82
                            ;
                            break;
                        case 31:
                            price = 40.47
                            ;
                            break;
                        case 32:
                            price = 41.12
                            ;
                            break;
                        case 33:
                            price = 41.77
                            ;
                            break;
                        case 34:
                            price = 42.42
                            ;
                            break;
                        case 35:
                            price = 43.07
                            ;
                            break;
                        case 36:
                            price = 43.71
                            ;
                            break;
                        case 37:
                            price = 44.36
                            ;
                            break;
                        case 38:
                            price = 45.01
                            ;
                            break;
                        case 39:
                            price = 45.66
                            ;
                            break;
                        case 40:
                            price = 46.31
                            ;
                            break;
                        case 41:
                            price = 46.96
                            ;
                            break;
                        case 42:
                            price = 47.61
                            ;
                            break;
                        case 43:
                            price = 48.26
                            ;
                            break;
                        case 44:
                            price = 48.91
                            ;
                            break;
                        case 45:
                            price = 49.56
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1350) {
                    switch (elevatorOrder.getLayer()) {
                        case 30:
                            price = 42.46
                            ;
                            break;
                        case 31:
                            price = 43.12
                            ;
                            break;
                        case 32:
                            price = 43.78
                            ;
                            break;
                        case 33:
                            price = 44.44
                            ;
                            break;
                        case 34:
                            price = 45.10
                            ;
                            break;
                        case 35:
                            price = 45.76
                            ;
                            break;
                        case 36:
                            price = 46.42
                            ;
                            break;
                        case 37:
                            price = 47.08
                            ;
                            break;
                        case 38:
                            price = 47.74
                            ;
                            break;
                        case 39:
                            price = 48.40
                            ;
                            break;
                        case 40:
                            price = 49.06
                            ;
                            break;
                        case 41:
                            price = 49.72
                            ;
                            break;
                        case 42:
                            price = 50.38
                            ;
                            break;
                        case 43:
                            price = 51.04
                            ;
                            break;
                        case 44:
                            price = 51.70
                            ;
                            break;
                        case 45:
                            price = 52.36
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 1600) {
                    switch (elevatorOrder.getLayer()) {
                        case 30:
                            price = 44.77
                            ;
                            break;
                        case 31:
                            price = 45.45
                            ;
                            break;
                        case 32:
                            price = 46.13
                            ;
                            break;
                        case 33:
                            price = 46.82
                            ;
                            break;
                        case 34:
                            price = 47.50
                            ;
                            break;
                        case 35:
                            price = 48.18
                            ;
                            break;
                        case 36:
                            price = 48.86
                            ;
                            break;
                        case 37:
                            price = 49.54
                            ;
                            break;
                        case 38:
                            price = 50.23
                            ;
                            break;
                        case 39:
                            price = 50.91
                            ;
                            break;
                        case 40:
                            price = 51.59
                            ;
                            break;
                        case 41:
                            price = 52.27
                            ;
                            break;
                        case 42:
                            price = 52.95
                            ;
                            break;
                        case 43:
                            price = 53.64
                            ;
                            break;
                        case 44:
                            price = 54.32
                            ;
                            break;
                        case 45:
                            price = 55.00
                            ;
                            break;
                    }
                }
            }
            if (elevatorOrder.getWeight() <= 1000) {
                if (elevatorOrder.getCg() != null) {
                    double i = elevatorOrder.getCg();
                    if (i != 0) {
                        double j = i - 3.5;
                        price = price + 0.05 * j;
                    }
                }
                if (elevatorOrder.getJk() != null) {
                    double i = elevatorOrder.getJk();
                    if (i != 0) {
                        if (i < 100) {
                            price = price + 0.03;
                        }
                        if (i > 100) {
                            price = price + 0.06;
                        }
                    }
                }
                if (elevatorOrder.getStation()!=null){
                    if (elevatorOrder.getStation()!=0){
                        int i =elevatorOrder.getStation();
                        int j = elevatorOrder.getLayer();
                        int z = j - i;
                        price = price - z *0.1;
                    }
                }
                if (null!=elevatorOrder.getElevatorZfgpkgn()) {
                    if (elevatorOrder.getElevatorZfgpkgn() != 0) {
                        price = price + 0.2;
                    }
                }
                if (elevatorOrder.getElevatorZfgpkcm() !=null) {
                    if (elevatorOrder.getElevatorZfgpkcm() != 0) {
                        price = price + 0.05;
                    }
                }
                if (elevatorOrder.getBxgdmt() != null) {
                    if (elevatorOrder.getBxgdmt() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getPtgbxgqtm() != null) {
                    if (elevatorOrder.getPtgbxgqtm() != 0) {
                        price = price + 0.05;
                    }
                }
                if (elevatorOrder.getPtgbxgqx() != null) {
                    if (elevatorOrder.getPtgbxgqx() != 0) {
                        price = price + 0;
                    }
                }
                if(elevatorOrder.getJtmbxgjm() != null) {
                    if (elevatorOrder.getJtmbxgjm() != 0) {
                        price = price + 0.1;
                    }
                }
                if(elevatorOrder.getJtmtjjm() !=null) {
                    if (elevatorOrder.getJtmtjjm() != 0) {
                        price = price + 0.15;
                    }
                }
                if(elevatorOrder.getJtmbxgjmzk() != null) {
                    if (elevatorOrder.getJtmbxgjmzk() != 0) {
                        price = price + 0.2;
                    }
                }
                if(elevatorOrder.getJtmtjjmzk() !=null) {
                    if (elevatorOrder.getJtmtjjmzk() != 0) {
                        price = price + 0.25;
                    }
                }
                if (elevatorOrder.getJxbxgjm() != null) {
                    if (elevatorOrder.getJxbxgjm() != 0) {
                        price = price + 0.27;
                    }
                }
                if (elevatorOrder.getJxtjjm() != null) {
                    if (elevatorOrder.getJxtjjm() != 0) {
                        price = price + 0.4;
                    }
                }
                if (elevatorOrder.getJxbxgjmzk() !=null) {
                    if (elevatorOrder.getJxbxgjmzk() != 0) {
                        price = price + 0.3;
                    }
                }
                if (elevatorOrder.getJxtjjmzk() != null) {
                    if (elevatorOrder.getJxtjjmzk() != 0) {
                        price = price + 0.55;
                    }
                }
                if (elevatorOrder.getJxfwzk() != null) {
                    if (elevatorOrder.getJxfwzk() != 0) {
                        price = price + 0.27;
                    }
                }
                if (elevatorOrder.getBxgjd() !=null) {
                    if (elevatorOrder.getBxgjd() != 0) {
                        price = price + 0;
                    }
                }
                if (elevatorOrder.getDlsdbds() !=null) {
                    if (elevatorOrder.getDlsdbds() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getDlsdbph() != null) {
                    if (elevatorOrder.getDlsdbph() != 0) {
                        price = price + 0.3;
                    }
                }
                if (elevatorOrder.getBxgsusgjx() != null) {
                    if (elevatorOrder.getBxgsusgjx() != 0) {
                        price = price + 0.1;
                    }
                }
                if (elevatorOrder.getBxgsusgjtm() != null) {
                    if (elevatorOrder.getBxgsusgjtm() != 0) {
                        price = price + 0.02;
                    }
                }
                if (elevatorOrder.getBxgtgjx() != null) {
                    if (elevatorOrder.getBxgtgjx() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getBxgtgjtm() != null) {
                    if (elevatorOrder.getBxgtgjtm() != 0) {
                        price = price + 0.045;
                    }
                }
                if (elevatorOrder.getPsgbgjtm() != null) {
                    if (elevatorOrder.getPsgbgjtm() != 0) {
                        price = price + 0.02;
                    }
                }
                if (elevatorOrder.getWjfztdzk() != null) {
                    if (elevatorOrder.getWjfztdzk() != 0) {
                        price = price + 0.3;
                    }
                }
                if (elevatorOrder.getYthczx() != null) {
                    if (elevatorOrder.getYthczx() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getFczx() != null) {
                    if (elevatorOrder.getFczx() != 0) {
                        if (elevatorOrder.getLayer() <= 16) {
                            price = price + 0.18;
                        } else if (17 <= elevatorOrder.getLayer()) {
                            price = price + 0.26;
                        }
                    }
                }
                if (elevatorOrder.getWhxp() != null) {
                    if (elevatorOrder.getWhxp() != 0) {
                        int i = elevatorOrder.getLayer();
                        price = price + i * 0.01;
                    }
                }
                if (elevatorOrder.getDdxp() != null) {
                    if (elevatorOrder.getDdxp() != 0) {
                        price = price + 0.12;
                    }
                }
                if (elevatorOrder.getCsyjxsp() != null) {
                    if (elevatorOrder.getCsyjxsp() != 0) {
                        price = price + 0.13;
                    }
                }
                if (elevatorOrder.getLbyjxsp() != null) {
                    if (elevatorOrder.getLbyjxsp() != 0) {
                        price = price + 0.04;
                    }
                }
                if (elevatorOrder.getQxnfs() != null) {
                    if (elevatorOrder.getQxnfs() != 0) {
                        price = price + 0.02;
                    }
                }
                if (elevatorOrder.getYybzgn() != null) {
                    if (elevatorOrder.getYybzgn() != 0) {
                        price = price + 0.08;
                    }
                }
                if (elevatorOrder.getQkxt() != null) {
                    if (elevatorOrder.getQkxt() != 0) {
                        price = price + 0.5;
                    }
                }
                if (elevatorOrder.getCctvsb() != null) {
                    if (elevatorOrder.getCctvsb() != 0) {
                        price = price + 0.2;
                    }
                }
                if (elevatorOrder.getCctvdl() != null) {
                    if (elevatorOrder.getCctvdl() != 0) {
                        price = price + 0;
                    }
                }
                if (elevatorOrder.getEhygm() != null) {
                    if (elevatorOrder.getEhygm() != 0) {
                        price = price + 0.1;
                    }
                }
                if (elevatorOrder.getKtxtsbdl() != null) {
                    if (elevatorOrder.getKtxtsbdl() != 0) {
                        price = price + 0.4;
                    }
                }
                if (elevatorOrder.getKtxtsbln() != null) {
                    if (elevatorOrder.getKtxtsbln() != 0) {
                        price = price + 0.5;
                    }
                }
                if (elevatorOrder.getKtxtdl() != null) {
                    if (elevatorOrder.getKtxtdl() != 0) {
                        price = price + 0;
                    }
                }
                if (elevatorOrder.getTqkm() != null) {
                    if (elevatorOrder.getTqkm() != 0) {
                        price = price + 0.1;
                    }
                }
                if (elevatorOrder.getYbdzd() != null) {
                    if (elevatorOrder.getYbdzd() != 0) {
                        price = price + 0.05;
                    }
                }
                if (elevatorOrder.getYcjkgn() != null) {
                    if (elevatorOrder.getYcjkgn() != 0) {
                        price = price + 0.5;
                    }
                }
                if (elevatorOrder.getDzgzyxgn() != null) {
                    if (elevatorOrder.getDzgzyxgn() != 0) {
                        price = price + 0.75;
                    }
                }
                if (elevatorOrder.getDqftkzxt() != null) {
                    if (elevatorOrder.getDqftkzxt() != 0) {
                        price = price + 0;
                    }
                }
                if (elevatorOrder.getQxgt() != null) {
                    if (elevatorOrder.getQxgt() != 0) {
                        price = price + 0.5;
                    }
                }
                if (elevatorOrder.getQxzjgt() != null) {
                    if (elevatorOrder.getQxzjgt() != 0) {
                        price = price + 0.93;
                    }
                }
                if (elevatorOrder.getGtPt() != null) {
                    if (elevatorOrder.getGtPt() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getGtBxg() != null) {
                    if (elevatorOrder.getGtBxg() != 0) {
                        price = price + 0.20;
                    }
                }
                if (elevatorOrder.getZjgtPx() != null) {
                    if (elevatorOrder.getZjgtPx() != 0) {
                        price = price + 0.21;
                    }
                }
                if (elevatorOrder.getZjgtBxg() != null) {
                    if (elevatorOrder.getZjgtBxg() != 0) {
                        price = price + 0.3;
                    }
                }
                if (elevatorOrder.getFbsjJj() != null) {
                    if (elevatorOrder.getFbsjJj() != 0) {
                        if (elevatorOrder.getNumber() <= 10) {
                            price = price + 0.6;
                        }
                        if (elevatorOrder.getNumber() > 10) {
                            price = price + 0.3;
                        }
                    }
                }
            } else if (elevatorOrder.getWeight() >= 1150) {
                if (elevatorOrder.getStation()!=null){
                    if (elevatorOrder.getStation()!=0){
                        int i =elevatorOrder.getStation();
                        int j = elevatorOrder.getLayer();
                        int z = j - i;
                        price = price - z *0.1;
                    }
                }
                if (elevatorOrder.getCg() != null) {
                    double i = elevatorOrder.getCg();
                    if (i != 0) {
                        double j = i - 3.5;
                        price = price + 0.06 * j;
                    }
                }
                if (elevatorOrder.getJk() != null) {
                    double i = elevatorOrder.getJk();
                    if (i != 0) {
                        if (i < 100) {
                            price = price + 0.06;
                        }
                        if (i > 100) {
                            price = price + 0.12;
                        }
                    }
                }
                if (null!=elevatorOrder.getElevatorZfgpkgn()) {
                    if (elevatorOrder.getElevatorZfgpkgn() != 0) {
                        price = price + 0.25;
                    }
                }
                if (elevatorOrder.getElevatorZfgpkcm() !=null) {
                    if (elevatorOrder.getElevatorZfgpkcm() != 0) {
                        price = price + 0.07;
                    }
                }
                if (elevatorOrder.getBxgdmt() != null) {
                    if (elevatorOrder.getBxgdmt() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getPtgbxgqtm() != null) {
                    if (elevatorOrder.getPtgbxgqtm() != 0) {
                        price = price + 0.07;
                    }
                }
                if (elevatorOrder.getPtgbxgqx() != null) {
                    if (elevatorOrder.getPtgbxgqx() != 0) {
                        price = price + 0;
                    }
                }
                if(elevatorOrder.getJtmbxgjm() != null) {
                    if (elevatorOrder.getJtmbxgjm() != 0) {
                        price = price + 0.12;
                    }
                }
                if(elevatorOrder.getJtmtjjm() !=null) {
                    if (elevatorOrder.getJtmtjjm() != 0) {
                        price = price + 0.17;
                    }
                }
                if(elevatorOrder.getJtmbxgjmzk() != null) {
                    if (elevatorOrder.getJtmbxgjmzk() != 0) {
                        price = price + 0.25;
                    }
                }
                if(elevatorOrder.getJtmtjjmzk() !=null) {
                    if (elevatorOrder.getJtmtjjmzk() != 0) {
                        price = price + 0.28;
                    }
                }
                if (elevatorOrder.getJxbxgjm() != null) {
                    if (elevatorOrder.getJxbxgjm() != 0) {
                        price = price + 0.3;
                    }
                }
                if (elevatorOrder.getJxtjjm() != null) {
                    if (elevatorOrder.getJxtjjm() != 0) {
                        price = price + 0.44;
                    }
                }
                if (elevatorOrder.getJxbxgjmzk() !=null) {
                    if (elevatorOrder.getJxbxgjmzk() != 0) {
                        price = price + 0.35;
                    }
                }
                if (elevatorOrder.getJxtjjmzk() != null) {
                    if (elevatorOrder.getJxtjjmzk() != 0) {
                        price = price + 0.7;
                    }
                }
                if (elevatorOrder.getJxfwzk() != null) {
                    if (elevatorOrder.getJxfwzk() != 0) {
                        price = price + 0.3;
                    }
                }
                if (elevatorOrder.getBxgjd() !=null) {
                    if (elevatorOrder.getBxgjd() != 0) {
                        price = price + 0;
                    }
                }
                if (elevatorOrder.getDlsdbds() !=null) {
                    if (elevatorOrder.getDlsdbds() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getDlsdbph() != null) {
                    if (elevatorOrder.getDlsdbph() != 0) {
                        price = price + 0.3;
                    }
                }
                if (elevatorOrder.getBxgsusgjx() != null) {
                    if (elevatorOrder.getBxgsusgjx() != 0) {
                        price = price + 0.1;
                    }
                }
                if (elevatorOrder.getBxgsusgjtm() != null) {
                    if (elevatorOrder.getBxgsusgjtm() != 0) {
                        price = price + 0.02;
                    }
                }
                if (elevatorOrder.getBxgtgjx() != null) {
                    if (elevatorOrder.getBxgtgjx() != 0) {
                        price = price + 0.21;
                    }
                }
                if (elevatorOrder.getBxgtgjtm() != null) {
                    if (elevatorOrder.getBxgtgjtm() != 0) {
                        price = price + 0.06;
                    }
                }
                if (elevatorOrder.getPsgbgjtm() != null) {
                    if (elevatorOrder.getPsgbgjtm() != 0) {
                        price = price + 0.03;
                    }
                }
                if (elevatorOrder.getWjfztdzk() != null) {
                    if (elevatorOrder.getWjfztdzk() != 0) {
                        price = price + 0.88;
                    }
                }
                if (elevatorOrder.getYthczx() != null) {
                    if (elevatorOrder.getYthczx() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getFczx() != null) {
                    if (elevatorOrder.getFczx() != 0) {
                        if (elevatorOrder.getLayer() <= 16) {
                            price = price + 0.18;
                        } else if (17 <= elevatorOrder.getLayer()) {
                            price = price + 0.26;
                        }
                    }
                }
                if (elevatorOrder.getWhxp() != null) {
                    if (elevatorOrder.getWhxp() != 0) {
                        int i = elevatorOrder.getLayer();
                        price = price + i * 0.01;
                    }
                }
                if (elevatorOrder.getDdxp() != null) {
                    if (elevatorOrder.getDdxp() != 0) {
                        price = price + 0.15;
                    }
                }
                if (elevatorOrder.getCsyjxsp() != null) {
                    if (elevatorOrder.getCsyjxsp() != 0) {
                        price = price + 0.13;
                    }
                }
                if (elevatorOrder.getLbyjxsp() != null) {
                    if (elevatorOrder.getLbyjxsp() != 0) {
                        price = price + 0.04;
                    }
                }
                if (elevatorOrder.getQxnfs() != null) {
                    if (elevatorOrder.getQxnfs() != 0) {
                        price = price + 0.02;
                    }
                }
                if (elevatorOrder.getYybzgn() != null) {
                    if (elevatorOrder.getYybzgn() != 0) {
                        price = price + 0.08;
                    }
                }
                if (elevatorOrder.getQkxt() != null) {
                    if (elevatorOrder.getQkxt() != 0) {
                        price = price + 0.5;
                    }
                }
                if (elevatorOrder.getCctvsb() != null) {
                    if (elevatorOrder.getCctvsb() != 0) {
                        price = price + 0.2;
                    }
                }
                if (elevatorOrder.getCctvdl() != null) {
                    if (elevatorOrder.getCctvdl() != 0) {
                        price = price + 0;
                    }
                }
                if (elevatorOrder.getEhygm() != null) {
                    if (elevatorOrder.getEhygm() != 0) {
                        price = price + 0.1;
                    }
                }
                if (elevatorOrder.getKtxtsbdl() != null) {
                    if (elevatorOrder.getKtxtsbdl() != 0) {
                        price = price + 0.45;
                    }
                }
                if (elevatorOrder.getKtxtsbln() != null) {
                    if (elevatorOrder.getKtxtsbln() != 0) {
                        price = price + 0.6;
                    }
                }
                if (elevatorOrder.getKtxtdl() != null) {
                    if (elevatorOrder.getKtxtdl() != 0) {
                        price = price + 0;
                    }
                }
                if (elevatorOrder.getTqkm() != null) {
                    if (elevatorOrder.getTqkm() != 0) {
                        price = price + 0.1;
                    }
                }
                if (elevatorOrder.getYbdzd() != null) {
                    if (elevatorOrder.getYbdzd() != 0) {
                        price = price + 0.05;
                    }
                }
                if (elevatorOrder.getYcjkgn() != null) {
                    if (elevatorOrder.getYcjkgn() != 0) {
                        price = price + 0.5;
                    }
                }
                if (elevatorOrder.getDzgzyxgn() != null) {
                    if (elevatorOrder.getDzgzyxgn() != 0) {
                        price = price + 0.75;
                    }
                }
                if (elevatorOrder.getDqftkzxt() != null) {
                    if (elevatorOrder.getDqftkzxt() != 0) {
                        price = price + 0;
                    }
                }
                if (elevatorOrder.getQxgt() != null) {
                    if (elevatorOrder.getQxgt() != 0) {
                        price = price + 0.6;
                    }
                }
                if (elevatorOrder.getQxzjgt() != null) {
                    if (elevatorOrder.getQxzjgt() != 0) {
                        price = price + 1.2;
                    }
                }
                if (elevatorOrder.getGtPt() != null) {
                    if (elevatorOrder.getGtPt() != 0) {
                        price = price + 0.2;
                    }
                }
                if (elevatorOrder.getGtBxg() != null) {
                    if (elevatorOrder.getGtBxg() != 0) {
                        price = price + 0.25;
                    }
                }
                if (elevatorOrder.getZjgtPx() != null) {
                    if (elevatorOrder.getZjgtPx() != 0) {
                        price = price + 0.23;
                    }
                }
                if (elevatorOrder.getZjgtBxg() != null) {
                    if (elevatorOrder.getZjgtBxg() != 0) {
                        price = price + 0.31;
                    }
                }
                if (elevatorOrder.getFbsjJj() != null) {
                    if (elevatorOrder.getFbsjJj() != 0) {
                        if (elevatorOrder.getNumber() <= 10) {
                            price = price + 0.6;
                        }
                        if (elevatorOrder.getNumber() > 10) {
                            price = price + 0.3;
                        }
                    }
                }
            }


        } else if (elevatorOrder.getType() == 2) {
            if (elevatorOrder.getSpeed() == 1.0) {
                switch (elevatorOrder.getLayer()) {
                    case 2:
                        price = 16.81
                        ;
                        break;
                    case 3:
                        price = 17.19
                        ;
                        break;
                    case 4:
                        price = 17.58
                        ;
                        break;
                    case 5:
                        price = 17.96
                        ;
                        break;
                    case 6:
                        price = 18.35
                        ;
                        break;
                    case 7:
                        price = 18.73
                        ;
                        break;
                    case 8:
                        price = 19.12
                        ;
                        break;
                    case 9:
                        price = 19.50
                        ;
                        break;
                    case 10:
                        price = 19.89
                        ;
                        break;

                }
            } else if (elevatorOrder.getSpeed() == 1.5) {
                switch (elevatorOrder.getLayer()) {
                    case 5:
                        price = 18.34
                        ;
                        break;
                    case 6:
                        price = 18.72
                        ;
                        break;
                    case 7:
                        price = 19.11
                        ;
                        break;
                    case 8:
                        price = 19.49
                        ;
                        break;
                    case 9:
                        price = 19.88
                        ;
                        break;
                    case 10:
                        price = 20.26
                        ;
                        break;
                    case 11:
                        price = 20.81
                        ;
                        break;
                    case 12:
                        price = 21.36
                        ;
                        break;
                    case 13:
                        price = 21.91
                        ;
                        break;
                    case 14:
                        price = 22.46
                        ;
                        break;
                    case 15:
                        price = 23.01
                        ;
                        break;
                    case 16:
                        price = 23.56
                        ;
                        break;
                    case 17:
                        price = 24.11
                        ;
                        break;
                    case 18:
                        price = 24.66
                        ;
                        break;
                    case 19:
                        price = 25.21
                        ;
                        break;
                    case 20:
                        price = 25.76
                        ;
                        break;
                }

            } else if (elevatorOrder.getSpeed() == 1.75) {
                switch (elevatorOrder.getLayer()) {
                    case 10:
                        price = 20.26
                        ;
                        break;
                    case 11:
                        price = 20.81
                        ;
                        break;
                    case 12:
                        price = 21.36
                        ;
                        break;
                    case 13:
                        price = 21.91
                        ;
                        break;
                    case 14:
                        price = 22.46
                        ;
                        break;
                    case 15:
                        price = 23.01
                        ;
                        break;
                    case 16:
                        price = 23.56
                        ;
                        break;
                    case 17:
                        price = 24.11
                        ;
                        break;
                    case 18:
                        price = 24.66
                        ;
                        break;
                    case 19:
                        price = 25.21
                        ;
                        break;
                    case 20:
                        price = 25.76
                        ;
                        break;
                    case 21:
                        price = 26.31
                        ;
                        break;
                    case 22:
                        price = 26.86
                        ;
                        break;
                    case 23:
                        price = 27.41
                        ;
                        break;
                    case 24:
                        price = 27.96
                        ;
                        break;
                    case 25:
                        price = 28.51
                        ;
                        break;
                    case 26:
                        price = 29.06
                        ;
                        break;
                    case 27:
                        price = 29.61
                        ;
                        break;
                    case 28:
                        price = 30.16
                        ;
                        break;
                    case 29:
                        price = 30.71
                        ;
                        break;
                    case 30:
                        price = 31.26
                        ;
                        break;
                }

            }

        } else if (elevatorOrder.getType() == 3) {
            if (elevatorOrder.getWeight() == 240) {
                switch (elevatorOrder.getLayer()) {
                    case 2:
                        price = 10.56
                        ;
                        break;
                    case 3:
                        price = 11.11
                        ;
                        break;
                    case 4:
                        price = 11.66
                        ;
                        break;
                    case 5:
                        price = 12.21
                        ;
                }
            } else if (elevatorOrder.getWeight() == 320) {
                switch (elevatorOrder.getLayer()) {
                    case 2:
                        price = 10.78
                        ;
                        break;
                    case 3:
                        price = 11.33
                        ;
                        break;
                    case 4:
                        price = 11.88
                        ;
                        break;
                    case 5:
                        price = 12.43
                        ;
                }
            } else if (elevatorOrder.getWeight() == 400) {
                switch (elevatorOrder.getLayer()) {
                    case 2:
                        price = 11.00
                        ;
                        break;
                    case 3:
                        price = 11.55
                        ;
                        break;
                    case 4:
                        price = 12.10
                        ;
                        break;
                    case 5:
                        price = 12.65
                        ;
                }
            }
        } else if (elevatorOrder.getType() == 5) {
            if (elevatorOrder.getSpeed() == 0.5) {
                if (elevatorOrder.getWeight() == 1000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 10.12
                            ;
                            break;
                        case 3:
                            price = 10.67
                            ;
                            break;
                        case 4:
                            price = 11.22
                            ;
                            break;
                        case 5:
                            price = 11.77
                            ;
                            break;
                        case 6:
                            price = 12.32
                            ;
                            break;
                        case 7:
                            price = 12.87
                            ;
                            break;
                        case 8:
                            price = 13.42
                            ;
                            break;
                        case 9:
                            price = 13.97
                            ;
                            break;
                        case 10:
                            price = 14.52
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 2000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 12.10
                            ;
                            break;
                        case 3:
                            price = 12.83
                            ;
                            break;
                        case 4:
                            price = 13.55
                            ;
                            break;
                        case 5:
                            price = 14.28
                            ;
                            break;
                        case 6:
                            price = 15.00
                            ;
                            break;
                        case 7:
                            price = 15.73
                            ;
                            break;
                        case 8:
                            price = 16.46
                            ;
                            break;
                        case 9:
                            price = 17.18
                            ;
                            break;
                        case 10:
                            price = 17.91
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 3000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 14.30
                            ;
                            break;
                        case 3:
                            price = 15.22
                            ;
                            break;
                        case 4:
                            price = 16.15
                            ;
                            break;
                        case 5:
                            price = 17.07
                            ;
                            break;
                        case 6:
                            price = 18.00
                            ;
                            break;
                        case 7:
                            price = 18.92
                            ;
                            break;
                        case 8:
                            price = 19.84
                            ;
                            break;
                        case 9:
                            price = 20.77
                            ;
                            break;
                        case 10:
                            price = 21.69
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 5000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 21.34
                            ;
                            break;
                        case 3:
                            price = 22.88
                            ;
                            break;
                        case 4:
                            price = 24.42
                            ;
                            break;
                        case 5:
                            price = 25.96
                            ;
                            break;
                        case 6:
                            price = 27.50
                            ;
                            break;
                        case 7:
                            price = 29.04
                            ;
                            break;
                        case 8:
                            price = 30.58
                            ;
                            break;
                        case 9:
                            price = 32.12
                            ;
                            break;
                        case 10:
                            price = 33.66
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 7500) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 28.60
                            ;
                            break;
                        case 3:
                            price = 31.02
                            ;
                            break;
                        case 4:
                            price = 33.44
                            ;
                            break;
                        case 5:
                            price = 35.86
                            ;
                            break;
                        case 6:
                            price = 38.28
                            ;
                            break;
                        case 7:
                            price = 40.70
                            ;
                            break;
                        case 8:
                            price = 43.12
                            ;
                            break;
                        case 9:
                            price = 45.54
                            ;
                            break;
                        case 10:
                            price = 47.96
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 10000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 39.60
                            ;
                            break;
                        case 3:
                            price = 42.90
                            ;
                            break;
                        case 4:
                            price = 46.20
                            ;
                            break;
                        case 5:
                            price = 49.50
                            ;
                            break;
                        case 6:
                            price = 52.80
                            ;
                            break;
                        case 7:
                            price = 56.10
                            ;
                            break;
                        case 8:
                            price = 59.40
                            ;
                            break;
                        case 9:
                            price = 62.70
                            ;
                            break;
                        case 10:
                            price = 66.00
                            ;
                            break;
                    }
                }

            } else if (elevatorOrder.getSpeed() == 1.0) {
                if (elevatorOrder.getWeight() == 1000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 10.12
                            ;
                            break;
                        case 3:
                            price = 10.67
                            ;
                            break;
                        case 4:
                            price = 11.22
                            ;
                            break;
                        case 5:
                            price = 11.77
                            ;
                            break;
                        case 6:
                            price = 12.32
                            ;
                            break;
                        case 7:
                            price = 12.87
                            ;
                            break;
                        case 8:
                            price = 13.42
                            ;
                            break;
                        case 9:
                            price = 13.97
                            ;
                            break;
                        case 10:
                            price = 14.52
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 2000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 15.40
                            ;
                            break;
                        case 3:
                            price = 16.13
                            ;
                            break;
                        case 4:
                            price = 16.85
                            ;
                            break;
                        case 5:
                            price = 17.58
                            ;
                            break;
                        case 6:
                            price = 18.30
                            ;
                            break;
                        case 7:
                            price = 19.03
                            ;
                            break;
                        case 8:
                            price = 19.76
                            ;
                            break;
                        case 9:
                            price = 20.48
                            ;
                            break;
                        case 10:
                            price = 21.21
                            ;
                            break;
                    }
                } else if (elevatorOrder.getWeight() == 3000) {
                    switch (elevatorOrder.getLayer()) {
                        case 2:
                            price = 18.70
                            ;
                            break;
                        case 3:
                            price = 19.62
                            ;
                            break;
                        case 4:
                            price = 20.55
                            ;
                            break;
                        case 5:
                            price = 21.47
                            ;
                            break;
                        case 6:
                            price = 22.40
                            ;
                            break;
                        case 7:
                            price = 23.32
                            ;
                            break;
                        case 8:
                            price = 24.24
                            ;
                            break;
                        case 9:
                            price = 25.17
                            ;
                            break;
                        case 10:
                            price = 26.09
                            ;
                            break;
                    }
                }

            }
            if (elevatorOrder.getWeight() == 1000) {
                if (elevatorOrder.getStation()!=null){
                    if (elevatorOrder.getStation()!=0){
                        int i =elevatorOrder.getStation();
                        int j = elevatorOrder.getLayer();
                        int z = j - i;
                        price = price - z *0.15;
                    }
                }
                if (elevatorOrder.getCg() != null) {
                    Double i = elevatorOrder.getCg();
                    double j = i - 4.0;
                    price = price + 0.07 * j;
                }
                if (elevatorOrder.getWeight() != null) {
                    price = price + 0;
                }
                if (elevatorOrder.getWjfztdzk() != null) {
                    price = price + 0.5;
                }
                if (elevatorOrder.getQxgt() != null) {
                    price = price + 0.5;
                }
                if (elevatorOrder.getGtPt() != null) {
                    price = price + 0.3;
                }
                if (elevatorOrder.getCargoPkgzfsc() != null) {
                    price = price + 0;
                }
                if (elevatorOrder.getJk() != null) {
                    if (elevatorOrder.getJk() < 100) {
                        price = price + 0.07;
                    }
                    if (100 < elevatorOrder.getJk()) {
                        price = price + 0.14;
                    }
                }
                if (elevatorOrder.getPtgbxgqtm() != null) {
                    price = price + 0.14;
                }
                if (elevatorOrder.getPtgbxgqx() != null) {
                    price = price + 0.3;
                }
                if (elevatorOrder.getBxgjd() != null) {
                    price = price + 0.08;
                }
                if (elevatorOrder.getFczx() != null) {
                    price = price + 0.18;
                }
                if (elevatorOrder.getTqkm()!=null){
                    price = price + 0;
                }
                if (elevatorOrder.getFbsjJj()!=null){
                    price = price + 0.8;
                }
                if (elevatorOrder.getBxgsusgjx()!=null){
                    price = price + 0.21;
                }
                if (elevatorOrder.getBxgsusgjtm()!=null){
                    price = price + 0.054;
                }
                if (elevatorOrder.getBxgtgjx()!=null){
                    price = price + 0.102;
                }
                if (elevatorOrder.getBxgtgjtm()!=null){
                    price = price + 0.042;
                }


            } else if (elevatorOrder.getWeight() == 2000) {
                if (elevatorOrder.getStation()!=null){
                    if (elevatorOrder.getStation()!=0){
                        int i =elevatorOrder.getStation();
                        int j = elevatorOrder.getLayer();
                        int z = j - i;
                        price = price - z *0.15;
                    }
                }
                if (elevatorOrder.getCg() != null) {
                    Double i = elevatorOrder.getCg();
                    double j = i - 4.0;
                    price = price + 0.1 * j;
                }
                if (elevatorOrder.getWeight() != null) {
                    price = price + 0;
                }
                if (elevatorOrder.getWjfztdzk() != null) {
                    price = price + 0.9;
                }
                if (elevatorOrder.getQxgt() != null) {
                    price = price + 0.7;
                }
                if (elevatorOrder.getGtPt() != null) {
                    price = price + 0.35;
                }
                if (elevatorOrder.getCargoPkgzfsc() != null) {
                    price = price + 0.1;
                }
                if (elevatorOrder.getCargoPkgzfsccm()!=null){
                    price = price + elevatorOrder.getLayer() * 0.05;
                }
                if (elevatorOrder.getJk() != null) {
                    if (elevatorOrder.getJk() < 100) {
                        price = price + 0.09;
                    }
                    if (100 < elevatorOrder.getJk()) {
                        price = price + 0.18;
                    }
                }
                if (elevatorOrder.getPtgbxgqtm() != null) {
                    price = price + 0.16;
                }
                if (elevatorOrder.getPtgbxgqx() != null) {
                    price = price + 0.5;
                }
                if (elevatorOrder.getBxgjd() != null) {
                    price = price + 0.15;
                }
                if (elevatorOrder.getFczx() != null) {
                    price = price + 0.18;
                }
                if (elevatorOrder.getTqkm()!=null){
                    price = price + 0;
                }
                if (elevatorOrder.getFbsjJj()!=null){
                    price = price + 0.8;
                }
                if (elevatorOrder.getBxgsusgjx()!=null){
                    price = price + 0.42;
                }
                if (elevatorOrder.getBxgsusgjtm()!=null){
                    price = price + 0.12;
                }
                if (elevatorOrder.getBxgtgjx()!=null){
                    price = price + 0.15;
                }
                if (elevatorOrder.getBxgtgjtm()!=null){
                    price = price + 0.06;
                }

            }else if (elevatorOrder.getWeight() ==3000){
                if (elevatorOrder.getStation()!=null){
                    if (elevatorOrder.getStation()!=0){
                        int i =elevatorOrder.getStation();
                        int j = elevatorOrder.getLayer();
                        int z = j - i;
                        price = price - z *0.2;
                    }
                }
                if (elevatorOrder.getCg() != null) {
                    Double i = elevatorOrder.getCg();
                    double j = i - 4.0;
                    price = price + 0.12 * j;
                }
                if (elevatorOrder.getWeight() != null) {
                    price = price + 0;
                }
                if (elevatorOrder.getWjfztdzk() != null) {
                    price = price + 1.5;
                }
                if (elevatorOrder.getQxgt() != null) {
                    price = price + 1;
                }
                if (elevatorOrder.getGtPt() != null) {
                    price = price + 0.45;
                }
                if (elevatorOrder.getCargoPkgzfsc() != null) {
                    price = price + 0.2;
                }
                if (elevatorOrder.getCargoPkgzfsccm()!=null){
                    price = price + elevatorOrder.getLayer() * 0.06;
                }
                if (elevatorOrder.getJk() != null) {
                    if (elevatorOrder.getJk() < 100) {
                        price = price + 0.12;
                    }
                    if (100 < elevatorOrder.getJk()) {
                        price = price + 0.24;
                    }
                }
                if (elevatorOrder.getPtgbxgqtm() != null) {
                    price = price + 0.22;
                }
                if (elevatorOrder.getPtgbxgqx() != null) {
                    price = price + 0.75;
                }
                if (elevatorOrder.getBxgjd() != null) {
                    price = price + 0.2;
                }
                if (elevatorOrder.getFczx() != null) {
                    price = price + 0.18;
                }
                if (elevatorOrder.getTqkm()!=null){
                    price = price + 0;
                }
                if (elevatorOrder.getFbsjJj()!=null){
                    price = price + 0.8;
                }
                if (elevatorOrder.getBxgsusgjx()!=null){
                    price = price + 0.63;
                }
                if (elevatorOrder.getBxgsusgjtm()!=null){
                    price = price + 0.168;
                }
                if (elevatorOrder.getBxgtgjx()!=null){
                    price = price + 0.21;
                }
                if (elevatorOrder.getBxgtgjtm()!=null){
                    price = price + 0.078;
                }
            }else if (elevatorOrder.getWeight() == 5000){
                if (elevatorOrder.getStation()!=null){
                    if (elevatorOrder.getStation()!=0){
                        int i =elevatorOrder.getStation();
                        int j = elevatorOrder.getLayer();
                        int z = j - i;
                        price = price - z *0.2;
                    }
                }
                if (elevatorOrder.getCg() != null) {
                    Double i = elevatorOrder.getCg();
                    double j = i - 4.0;
                    price = price + 0.12 * j;
                }
                if (elevatorOrder.getWeight() != null) {
                    price = price + 0;
                }
                if (elevatorOrder.getWjfztdzk() != null) {
                    price = price + 2.4;
                }
                if (elevatorOrder.getQxgt() != null) {
                    price = price + 1.44;
                }
                if (elevatorOrder.getGtPt() != null) {
                    price = price + 0.56;
                }
                if (elevatorOrder.getCargoPkgzfsc() != null) {
                    price = price + 0;
                }
                if (elevatorOrder.getCargoPkgzfsccm()!=null){
                    price = price + elevatorOrder.getLayer() * 0;
                }
                if (elevatorOrder.getJk() != null) {
                    if (elevatorOrder.getJk() < 100) {
                        price = price + 0.15;
                    }
                    if (100 < elevatorOrder.getJk()) {
                        price = price + 0.3;
                    }
                }
                if (elevatorOrder.getPtgbxgqtm() != null) {
                    price = price + 0.36;
                }
                if (elevatorOrder.getPtgbxgqx() != null) {
                    price = price + 1.2;
                }
                if (elevatorOrder.getBxgjd() != null) {
                    price = price + 0.28;
                }
                if (elevatorOrder.getFczx() != null) {
                    price = price + 0.18;
                }
                if (elevatorOrder.getTqkm()!=null){
                    price = price + 0;
                }
                if (elevatorOrder.getFbsjJj()!=null){
                    price = price + 0.8;
                }
                if (elevatorOrder.getBxgsusgjx()!=null){
                    price = price + 1.08;
                }
                if (elevatorOrder.getBxgsusgjtm()!=null){
                    price = price + 0.228;
                }
                if (elevatorOrder.getBxgtgjx()!=null){
                    price = price + 0.3;
                }
                if (elevatorOrder.getBxgtgjtm()!=null){
                    price = price + 0.108;
                }
            }


        }


        return price;


    }

    @Override
    public PageUtils queryPageElevator(Map<String, Object> params) {
        String username = (String) params.get("username");
        IPage<ElevatorOrderEntity> page = this.page(
                new Query<ElevatorOrderEntity>().getPage(params),
                new QueryWrapper<ElevatorOrderEntity>().eq("username",username).orderByDesc("id")
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageElevatorType(Map<String, Object> params,Integer type) {
        String username = (String) params.get("username");
        IPage<ElevatorOrderEntity> page = this.page(
                new Query<ElevatorOrderEntity>().getPage(params),
                new QueryWrapper<ElevatorOrderEntity>().eq("type",type).eq("username",username)
        );
        return new PageUtils(page);
    }

}
