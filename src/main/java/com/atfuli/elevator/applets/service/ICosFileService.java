package com.atfuli.elevator.applets.service;

import com.atfuli.elevator.applets.common.utils.R;
import org.springframework.web.multipart.MultipartFile;

public interface ICosFileService {

     R upload(MultipartFile file);
}
