package com.atfuli.elevator.applets.dao;

import com.atfuli.elevator.applets.entity.BookEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author hzc
 * @email 
 * @date 2022-12-09 16:21:17
 */
@Mapper
public interface BookDao extends BaseMapper<BookEntity> {
	
}
