package com.atfuli.elevator.applets.dao;

import com.atfuli.elevator.applets.entity.ElevatorTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author hzc
 * @email 
 * @date 2023-01-06 17:35:42
 */
@Mapper
public interface ElevatorTypeDao extends BaseMapper<ElevatorTypeEntity> {
	
}
