package com.atfuli.elevator.applets.dao;

import com.atfuli.elevator.applets.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author hzc
 * @email 
 * @date 2023-01-04 10:48:49
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
