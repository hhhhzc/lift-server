package com.atfuli.elevator.applets.dao;

import com.atfuli.elevator.applets.entity.ElevatorOrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author hzc
 * @email 
 * @date 2023-01-03 13:47:24
 */
@Mapper
public interface ElevatorOrderDao extends BaseMapper<ElevatorOrderEntity> {
	
}
