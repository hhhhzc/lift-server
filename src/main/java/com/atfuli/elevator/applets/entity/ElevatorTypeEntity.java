package com.atfuli.elevator.applets.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author hzc
 * @email 
 * @date 2023-01-06 17:35:42
 */
@Data
@TableName("elevator_type")
public class ElevatorTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer type;
	/**
	 * 
	 */
	private Double speed;
	/**
	 * 
	 */
	private Integer weight;
	/**
	 * 
	 */
	private Integer layer;
	/**
	 * 
	 */
	private Integer station;
	/**
	 * 
	 */
	private Integer door;

}
