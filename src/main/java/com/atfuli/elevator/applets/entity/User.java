package com.atfuli.elevator.applets.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class User {


    @TableId
    private String openId;
    /**
     *
     */
    private String avatar;
    /**
     *
     */
    private String nickName;
    /**
     *
     */

}
