package com.atfuli.elevator.applets.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 *
 * @author hzc
 * @email
 * @date 2023-01-03 13:47:24
 */
@Data
@TableName("elevator_order")
public class ElevatorOrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Integer id;
	/**
	 *
	 */
	private Integer type;
	/**
	 *
	 */
	private Double speed;
	/**
	 *
	 */
	private Integer weight;
	/**
	 *
	 */
	private Integer layer;
	/**
	 *
	 */
	private Integer station;
	/**
	 *
	 */
	private Integer door;
	/**
	 *
	 */
	private Integer ywGt;
	/**
	 *
	 */
	private Integer ywJf;
	/**
	 * 折扣
	 */
	private Double discount;
	/**
	 * 数量
	 */
	private Integer number;
	/**
	 *
	 */
	private Double cg;
	/**
	 *
	 */
	private Double jk;
	/**
	 *
	 */
	private Integer elevatorZfgpkgn;
	/**
	 *
	 */
	private Integer elevatorZfgpkcm;
	/**
	 *
	 */
	private Integer cargoPkgzfsc;
	/**
	 *
	 */
	private Integer cargoPkgzfsccm;
	/**
	 *
	 */
	private Integer bxgdmt;
	/**
	 *
	 */
	private Integer ptgbxgqtm;
	/**
	 *
	 */
	private Integer ptgbxgqx;
	/**
	 *
	 */
	private Integer jtmbxgjm;
	/**
	 *
	 */
	private Integer jtmtjjm;
	/**
	 *
	 */
	private Integer jtmbxgjmzk;
	/**
	 *
	 */
	private Integer jtmtjjmzk;
	/**
	 *
	 */
	private Integer jxbxgjm;
	/**
	 *
	 */
	private Integer jxtjjm;
	/**
	 *
	 */
	private Integer jxbxgjmzk;
	/**
	 *
	 */
	private Integer jxtjjmzk;
	/**
	 *
	 */
	private Integer jxfwzk;
	/**
	 *
	 */
	private Integer bxgjd;
	/**
	 *
	 */
	private Integer dlsdbds;
	/**
	 *
	 */
	private Integer dlsdbph;
	/**
	 *
	 */
	private Integer bxgsusgjx;
	/**
	 *
	 */
	private Integer bxgsusgjtm;
	/**
	 *
	 */
	private Integer bxgtgjx;
	/**
	 *
	 */
	private Integer bxgtgjtm;
	/**
	 *
	 */
	private Integer psgbgjtm;
	/**
	 *
	 */
	private Integer wjfztdzk;
	/**
	 *
	 */
	private Integer ythczx;
	/**
	 *
	 */
	private Integer fczx;
	/**
	 *
	 */
	private Integer whxp;
	/**
	 *
	 */
	private Integer ddxp;
	/**
	 *
	 */
	private Integer csyjxsp;
	/**
	 *
	 */
	private Integer lbyjxsp;
	/**
	 *
	 */
	private Integer qxnfs;
	/**
	 *
	 */
	private Integer yybzgn;
	/**
	 *
	 */
	private Integer qkxt;
	/**
	 *
	 */
	private Integer cctvsb;
	/**
	 *
	 */
	private Integer cctvdl;
	/**
	 *
	 */
	private Integer ehygm;
	/**
	 *
	 */
	private Integer ktxtsbdl;
	/**
	 *
	 */
	private Integer ktxtsbln;
	/**
	 *
	 */
	private Integer ktxtdl;
	/**
	 *
	 */
	private Integer tqkm;
	/**
	 *
	 */
	private Integer ybdzd;
	/**
	 *
	 */
	private Integer ycjkgn;
	/**
	 *
	 */
	private Integer dzgzyxgn;
	/**
	 *
	 */
	private Integer dqftkzxt;
	/**
	 *
	 */
	private Integer qxgt;
	/**
	 *
	 */
	private Integer qxzjgt;
	/**
	 *
	 */
	private Integer gtPt;
	/**
	 *
	 */
	private Integer gtBxg;
	/**
	 *
	 */
	private Integer zjgtPx;
	/**
	 *
	 */
	private Integer zjgtBxg;
	/**
	 *
	 */
	private Integer fbsjYs;
	/**
	 *
	 */
	private Integer fbsjYx;
	/**
	 *
	 */
	private Integer fbsjJj;


	private Date createdAt;
	/**
	 *
	 */
	private Date updatedAt;
	/**
	 *
	 */
	@TableLogic
	private Integer deletedAt;

	private Double price;

	private String username;


}
