package com.atfuli.elevator.applets.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 *
 * @author hzc
 * @email
 * @date 2023-01-04 10:48:49
 */
@Data
@TableName("user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private String id;
	/**
	 *
	 */
	private String username;
	/**
	 *
	 */
	private Integer password;

	private Date createdAt;
	/**
	 *
	 */
	private Date updatedAt;

}
