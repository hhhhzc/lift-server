package com.atfuli.elevator.applets.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 *
 *
 * @author hzc
 * @email
 * @date 2022-12-09 16:21:17
 */
@Data
@TableName("hhh_book")
public class BookEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Integer id;
	/**
	 *
	 */
	private String name;
	/**
	 *
	 */
	private String type;
	/**
	 *
	 */
	private String author;

	/**
	 * 逻辑删除
	 */
	@TableLogic
	private String status;

}
