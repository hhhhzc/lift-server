package com.atfuli.elevator.applets.controller;

import java.util.Arrays;
import java.util.Map;

import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.atfuli.elevator.applets.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atfuli.elevator.applets.entity.ElevatorTypeEntity;
import com.atfuli.elevator.applets.service.ElevatorTypeService;



/**
 *
 *
 * @author hzc
 * @email
 * @date 2023-01-06 17:35:42
 */
@RestController
@RequestMapping("applets/elevatortype")
public class ElevatorTypeController {
    @Autowired
    private ElevatorTypeService elevatorTypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = elevatorTypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		ElevatorTypeEntity elevatorType = elevatorTypeService.getById(id);

        return R.ok().put("elevatorType", elevatorType);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ElevatorTypeEntity elevatorType){
		elevatorTypeService.save(elevatorType);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ElevatorTypeEntity elevatorType){
		elevatorTypeService.updateById(elevatorType);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		elevatorTypeService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
