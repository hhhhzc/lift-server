package com.atfuli.elevator.applets.controller;


import com.atfuli.elevator.applets.common.utils.R;
import com.atfuli.elevator.applets.service.ICosFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Api(tags = "文件的api")
@RequestMapping("/file")
@Validated
public class CosFileController {

    @Autowired
    private ICosFileService cosFileService;

    /**
     * 上传文件
     * @param file
     */
    @RequestMapping(value="/fillLoad",method = RequestMethod.POST)
    @ApiImplicitParam(name = "file", value = "文件", dataType = "MultipartFile")
    @ApiOperation(value = "上传文件", httpMethod = "POST")
    public R addUser(@RequestPart("file") MultipartFile file) {
        cosFileService.upload(file);
        return R.ok();
    }



}
