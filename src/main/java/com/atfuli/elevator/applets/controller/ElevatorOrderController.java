package com.atfuli.elevator.applets.controller;

import java.util.Arrays;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atfuli.elevator.applets.common.utils.PageUtils;
import com.atfuli.elevator.applets.common.utils.R;
import com.atfuli.elevator.applets.common.utils.WechatUtil;
import com.atfuli.elevator.applets.entity.ElevatorOrderEntity;
import com.atfuli.elevator.applets.entity.User;
import com.atfuli.elevator.applets.entity.UserEntity;
import com.atfuli.elevator.applets.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.atfuli.elevator.applets.service.ElevatorOrderService;

import javax.servlet.http.HttpServletRequest;


/**
 *
 *
 * @author hzc
 * @email
 * @date 2023-01-03 13:47:24
 */

@CrossOrigin
@RestController
@Slf4j
@Api(tags = "ElevatorOrderController", description = "SwaggerController | 测试swagger")
@RequestMapping("applets/elevatororder")
public class ElevatorOrderController {
    @Autowired
    private ElevatorOrderService elevatorOrderService;

    @Autowired
    private UserService userService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = elevatorOrderService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id){
		ElevatorOrderEntity elevatorOrder = elevatorOrderService.getById(id);

        return R.ok().put("elevatorOrder", elevatorOrder);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ElevatorOrderEntity elevatorOrder){
		elevatorOrderService.save(elevatorOrder);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody ElevatorOrderEntity elevatorOrder){
		elevatorOrderService.updateById(elevatorOrder);

        return R.ok();
    }

    /**
     * 删除
     */
    @GetMapping("/delete")
    public R delete(@RequestBody Integer[] ids){
		elevatorOrderService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    @ApiOperation(value="计算价格方法", notes="计算价格")
    @PostMapping("/inquiry")
    public R inquirt(@RequestBody ElevatorOrderEntity elevatorOrder){
        double v = elevatorOrderService.calculatePrice(elevatorOrder);
        elevatorOrder.setPrice(v);
        elevatorOrderService.save(elevatorOrder);
        return R.ok().put("price",v);
    }



    /**
     * 列表
     */
    @ApiOperation(value="查询方法", notes="查询方法")
    @PostMapping("/listele")
    public R listElevator(@RequestBody Map<String, Object> params){
        PageUtils page = elevatorOrderService.queryPageElevator(params);

        return R.ok().put("page", page);
    }

    @ApiOperation(value="查询单个电梯方法", notes="查询方法")
    @PostMapping("/listtype")
    public R listElevatorType(@RequestBody Map<String, Object> params,
                              @RequestParam(value = "type") Integer type){
        PageUtils page = elevatorOrderService.queryPageElevatorType(params,type);

        return R.ok().put("page", page);
    }

    @ApiOperation(value="登录", notes="登录")
    @GetMapping("/user/login")
    public R logo(HttpServletRequest request){
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UserEntity user = userService.getOne(new QueryWrapper<UserEntity>().eq("username", username));
        if (user!=null){
            UserEntity pass = userService.getOne(new QueryWrapper<UserEntity>().eq("password", password));
            if (pass!=null){
                return R.ok("登录成功");
            }else {
                return R.error(404,"登录失败");
            }
        }
        else {
            return R.error(404,"登录失败");
        }
    }

    @ApiOperation(value="小程序登录", notes="登录测试")
    @GetMapping("/wx/login")
    public R user_login(@RequestParam(value = "code", required = false) String code,
                        @RequestParam(value = "rawDate", required = false) String rawData,
                        @RequestParam(value = "signature", required = false) String signature) {
        // 用户非敏感信息：rawData
        // 签名：signature
        JSONObject rawDataJson = JSON.parseObject(rawData);
        // 1.接收小程序发送的code
        // 2.开发者服务器 登录凭证校验接口 appi + appsecret + code
        JSONObject SessionKeyOpenId = WechatUtil.getSessionKeyOrOpenId(code);
        // 3.接收微信接口服务 获取返回的参数
        String openid = SessionKeyOpenId.getString("openid");
        String sessionKey = SessionKeyOpenId.getString("session_key");

        // 4.校验签名 小程序发送的签名signature与服务器端生成的签名signature2 = sha1(rawData + sessionKey)
        String signature2 = DigestUtils.sha1Hex(rawData + sessionKey);
        if (!signature.equals(signature2)) {
            return R.error("签名校验失败");
        }
        // 5.根据返回的User实体类，判断用户是否是新用户，是的话，将用户信息存到数据库；
        LambdaQueryWrapper<UserEntity> lqw = Wrappers.lambdaQuery();
        lqw.eq(UserEntity::getId, openid);
        UserEntity user = userService.getOne(lqw);
        if (user == null) {
            // 用户信息入库
            String username = rawDataJson.getString("username");
            String password = rawDataJson.getString("password");
            user = new UserEntity();
            user.setId(openid);
            user.setUsername(username);
            userService.save(user);
        }
        return R.ok().put("user",user);
    }

}
